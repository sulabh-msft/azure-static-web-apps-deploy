# **GitLab Pipeline: Azure Static Web Apps Deploy**

Deploys an application to [Azure Static Web Apps](https://azure.microsoft.com/en-gb/services/app-service/static/).

**Prerequisites**

You’ll need to validate your account with a credit card. This is required to discourage and reduce abuse on GitLab infrastructure.

### **YAML Definition**
Add the following snippet to the script section of your `.gitlab-ci.yml` file:
```
variables:
  API_TOKEN: $DEPLOYMENT_TOKEN
  APP_PATH: '$CI_PROJECT_DIR/<string>'
  API_PATH: '$CI_PROJECT_DIR/<string>'     # Optional
  OUTPUT_PATH: '$CI_PROJECT_DIR/<string>'  # Optional

deploy:
  stage: deploy
  image : registry.gitlab.com/static-web-apps/azure-static-web-apps-deploy
  script:
    - echo "App deployed successfully."
```

### **Variable**
| Variable | Usage |
| ------ | ------ |
| API_TOKEN(*)| API token for deployment |
| APP_PATH(*) | Directory of app source code |
| API_PATH | Directory of api source code |
| OUTPUT_PATH | Directory of built application |

_(*) = required variable._

*Note:
1. `$CI_PROJECT_DIR` is the full path the repository is cloned to, and where the job runs from. 
2. If you are not using the sample app, the values for `APP_PATH`, and `OUTPUT_PATH` need to change to match the values in your application. Note that you must give the values for `APP_PATH`, `OUTPUT_PATH` only after `$CI_PROJECT_DIR` as shown above. i.e., `APP_PATH: $CI_PROJECT_DIR/<app location>`.

### **Add deployment token**
1. Copy deployment token from your static web app resource from [Azure](https://ms.portal.azure.com/#home) portal under manage deployment token.
2. Go to Settings -> CI/CD in your project on GitLab.
3. Expand variables section.
4. Select Add variable.
5. Put `Key = DEPLOYMENT_TOKEN`.
6. Put `Value = <Token copied in step 1>`.
7. Select Add variable.

### **Example-1***
```
variables:
  API_TOKEN: $DEPLOYMENT_TOKEN
  APP_PATH: '$CI_PROJECT_DIR/src'

deploy:
  stage: deploy
  image : registry.gitlab.com/static-web-apps/azure-static-web-apps-deploy
  script:
    - echo "App deployed successfully."
```

###### *Example shown above has deployed [Vanilla App](https://gitlab.com/static-web-apps/vanilla-basic-sample) to [Static Web App](https://ms.portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.Web%2FStaticSites).

### **Example-2***
```
variables:
  API_TOKEN: $DEPLOYMENT_TOKEN
  APP_PATH: '$CI_PROJECT_DIR/src'
  API_PATH: '$CI_PROJECT_DIR/api'

deploy:
  stage: deploy
  image : registry.gitlab.com/static-web-apps/azure-static-web-apps-deploy
  script:
    - echo "App deployed successfully."
```

###### *Example shown above has deployed [Vanilla Api](https://gitlab.com/static-web-apps/vanilla-api-sample) to [Static Web App](https://ms.portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.Web%2FStaticSites).

### **Other optional paramters**
| Variable | Usage |
| ------ | ------ |
| VERBOSE | (Default: false) Enables verbose logging |
| WOR_DIR	| Working directory of the repository |
| BUILD_TIM_MIN | Time limit of oryx build in minutes |
| ROUTE_PATH | Path to the routes file |
| CONFIG_PATH | Path to the staticwebapp.config.json |
| DATA_API_PATH | Directory of the data Api configuration files |
| ARTIFACT_PATH | Directory of built application artifacts |
| DEPLOYMENT_ENVIRONMENT | Deployment Environment |
| EVENT_PATH | Filepath of the event json |
| REPO_TOKEN | RepoToken |
| APP_BUILD_COMMAND | App Build Command |
| API_BUILD_COMMAND | Api Build Command |
| REPO_URL | Repository Url |
| BRANCH | Branch |
| SKIP_APP_BUILD | (Default: false) Skips Oryx build for app folder |
| SKIP_API_BUILD | (Default: false) Skips Oryx build for api folder |
| PR_TITLE | Pull request title for staging sites |
| HEAD_BRANCH | Head branch name for staging sites |
| PROD_BRANCH | Production branch. When specified, deployments from other branches will be staging environments |
| VERSION | Display version information |
| HELP | Display this help screen |


**Support** \
This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance.
